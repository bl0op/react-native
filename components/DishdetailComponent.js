import React, { Component, useRef } from 'react';
import { View, Text, ScrollView, SafeAreaView, FlatList, Modal, Button, StyleSheet, Alert, PanResponder, Share } from 'react-native';
import { Card, Icon, AirbnbRating, Rating, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite, postComment } from '../redux/ActionCreators';
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
	return {
		dishes: state.dishes,
		comments: state.comments,
		favorites: state.favorites
	}
}

const mapDispatchToProps = dispatch => ({
	postFavorite: (dishId) => dispatch(postFavorite(dishId)),
	postComment: (values) => dispatch(postComment(values))
})


const styles = StyleSheet.create({
	iconsRow: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center'
	},
	modal: {
		padding: 20,
		flex: 1
	},
	modalRow: {
		margin: 10
	},
	comment: {
		flex: 1,
		margin: 10,
		alignItems: 'flex-start'
	}
});

const ShareDish = (title, message, url) => {
	Share.share({
		title: title,
		message: title + ': ' + message + ' ' + url,
		url: url
	}, {
		dialogTitle: 'Share ' + title
	});
}


const RenderDish = (props) => {
	const dish = props.dish;

	const viewEl = useRef(null);

	const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
		if( dx < -100 )
			return 'rightToLeft';
		else if( dx > 100 )
			return 'leftToRight';
		else
			return null;
	};

	const panResponder = PanResponder.create({
		onStartShouldSetPanResponder: (e, gestureState) => {
			return true;
		},
		onPanResponderGrant: () => {
			viewEl.current.rubberBand(1000)
				.then(endState => console.log(endState.finished ? "finished" : "canclelled" ));
		},
		onPanResponderEnd: (e, gestureState) => {
			switch(recognizeDrag(gestureState)){
				case 'rightToLeft':
					Alert.alert(
						'Add to Favorites',
						'Are you sure you wish to ass ' + dish.name + ' to your favorites?',
						[
						{
								text: 'CANCEL',
								onPress: () => console.log('Cancel pressed'),
								style: 'cancel'
							},
							{
								text: 'OK',
								onPress: () => props.favorite ? console.log('Already favorite') : props.onPress()
							}
						],
						{ cancelable: false }
					);
					break;
				case 'leftToRight':
					props.onComment();
					break;
				default:
					console.log('Unrecognizable drag');
			}
			return true;
		}
	});

	if (dish != null) {
		return(
			<Animatable.View animation="fadeInDown"
				duration={2000}
				delay={1000}
				ref={viewEl}
				{...panResponder.panHandlers}
			>
				<Card
					featuredTitle={dish.name}
					image={{ uri: baseUrl + dish.image }}
				>
					<Text style={{margin: 10}}>
						{dish.description}
					</Text>
					<View style={styles.iconsRow}>
					<Icon
						raised
						reverse
						name={ props.favorite ? 'heart' : 'heart-o'}
						type='font-awesome'
						color='#f50'
						onPress={() => props.favorite ? console.log('Already favorite'): props.onFavorite()}
					/>
					<Icon
						raised
						reverse
						name={ 'pencil'}
						type='font-awesome'
						color='#512DA8'
						onPress={() =>{ props.onComment()}}
					/>
					<Icon
						raised
						reverse
						name={ 'share'}
						type='font-awesome'
						color='#512DA8'
						onPress={() => ShareDish(dish.name, dish.description, baseUrl + dish.image)}
					/>
					</View>
				</Card>
			</Animatable.View>
		);
	}
	else {
		return(<View></View>);
	}
}

const RenderComments = (props) => {
	const comments = props.comments;

	const renderCommentItem = ({ item, index}) => {
		return(
			<View key={index} style={styles.comment}>
				<Text style={{fontSize: 14}}>{item.comment}</Text>
				<Rating imageSize={14} readonly defaultValue={item.rating.toString()}/>
				<Text style={{fontSize: 12}}>{'-- ' + item.author + ', ' + item.date}</Text>
			</View>
		);
	}
	
	return(
		<Animatable.View animation="fadeInDown"
			duration={2000}
			delay={1000}
		>
			<Card title="Comment">
				<SafeAreaView>
					<FlatList
						data={comments}
						renderItem={renderCommentItem}
						keyExtractor={item => item.id.toString()}
					/>
				</SafeAreaView>
			</Card>
		</Animatable.View>
	);
}

class CommentModal extends Component {
	constructor(props){
		super(props);
		this.state = {
			rating: 1,
			author: '',
			comment: ''
		}
	}

	resetModal(){
		this.setState({
			rating: 1,
			author: '',
			comment: ''
		});
	}
	
	render(){
		return(
			<Modal
				animationType={'slide'}
				transparent={false}
				visible={this.props.showModal}
				onDismiss={() => { this.props.toggleModal(); this.resetModal();}}
				onRequestClose={() => { this.props.toggleModal(); this.resetModal();}}
			>
				<View
					style={styles.modal}
				>
					<View style={styles.modalRow}>
						<AirbnbRating
							showRating
							ratingCount={5}
							reviews={[1,2,3,4,5]}
							style={styles.modalItem}
							defaultRating={this.state.rating}
							onFinishRating={(value) => this.setState({rating: value})}
						/>
					</View>
					<View style={styles.modalRow}>
						<Input
							placeholder="Author"
							leftIcon={{type: 'font-awesome', name: 'user', margin: 10}}
							value={this.state.author}
							onChangeText={(value) => this.setState({ author: value })}
						/>
					</View>
					<View style={styles.modalRow}>
						<Input
							placeholder="Comment"
							leftIcon={{type: 'font-awesome', name: 'comment', margin: 10}}
							value={this.state.comment}
							onChangeText={(value) => this.setState({ comment: value })}
						/>
					</View>
					<View style={styles.modalRow}>
						<Button
							onPress={() => { this.props.handleSubmit(this.state); this.resetModal();}}
							color='#512DA8'
							title='SUBMIT'
							style={styles.modalItem}
						/>
					</View>
					<View style={styles.modalRow}>
						<Button
							onPress={() => { this.props.toggleModal(); this.resetModal();}}
							color='#C0C0C0'
							title='CANCEL'
							style={styles.modalItem}
						/>
					</View>
				</View>
			</Modal>
		);
	}
}


class Dishdetail extends Component {

	constructor(props){
		super(props);
		this.state = {
			showModal: false
		};
	}

	markFavorite(dishId){
		this.props.postFavorite(dishId);
	}

	toggleModal(){
		this.setState({
			showModal: !this.state.showModal
		});
	}

	handleSubmit(values){
		this.toggleModal();
		values.dishId = this.props.route.params.dishId;	
		this.props.postComment(values);
	}

	render() {
		const dishId = this.props.route.params.dishId;
		var dish = this.props.dishes.dishes.filter((item)=>item.id === dishId)[0];
		return(
			<ScrollView>
				<RenderDish
					dish={dish}
					favorite={this.props.favorites.some(el => el === dishId)}
					onFavorite={() => this.markFavorite(dishId)}
					onComment={() => this.toggleModal()}
				/>
				<RenderComments comments={this.props.comments.comments.filter((comment)=>dish.id === comment.dishId)} />
				<CommentModal toggleModal={() => this.toggleModal()} handleSubmit={(values) => this.handleSubmit(values)} showModal={this.state.showModal} />
			</ScrollView>
		);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail);
