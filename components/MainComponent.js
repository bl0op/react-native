import 'react-native-gesture-handler';
import Constants from 'expo-constants';
import React, { Component } from 'react';
import { View, Text, Platform, Image, StyleSheet, SafeAreaView, ToastAndroid } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerItemList, DrawerContentScrollView } from '@react-navigation/drawer';
import Auth from './AuthComponent';
import Home from './HomeComponent';
import Contacts from './ContactsComponent';
import AboutUs from './AboutUsComponent';
import Menu from './MenuComponent';
import Dishdetail from './DishdetailComponent';
import Reservation from './ReservationComponent';
import Favorites from './FavoritesComponent';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { fetchDishes, fetchComments, fetchPromos, fetchLeaders } from '../redux/ActionCreators';
import NetInfo from "@react-native-community/netinfo";

const mapStateToProps = state => {
	return {
	}
}

const mapDispatchToProps = dispatch => ({
	fetchDishes: () => dispatch(fetchDishes()),
	fetchComments: () => dispatch(fetchComments()),
	fetchPromos: () => dispatch(fetchPromos()),
	fetchLeaders: () => dispatch(fetchLeaders())
});

const drawerStyle = {
	backgroundColor: '#D1C4E9'
}

const drawerOptions = (iconName) => {
	return ({
	drawerIcon: ({ tintColor }) => (
		<Icon
			name={iconName}
			type='font-awesome'
			size={iconName === 'address-card' ? 21 : 24}
			color={tintColor}
		/>
	)});
}

const stackOptions = (navigation) => {
	return {
		headerStyle: {
			backgroundColor: '#512DA8'
		},
		headerTintColor: '#fff',
		headerTitleStyle: {
			color: '#fff'
		},
		headerLeft: () =>
					<Icon name='menu'
					size={24}
					iconStyle={{
						color: 'white',
						marginLeft: 15
					}}
					onPress={() => navigation.toggleDrawer()}
					/>
	}
}

const MainNavigator = createDrawerNavigator();
const AuthNavigator = createStackNavigator();
const HomeNavigator = createStackNavigator();
const MenuNavigator = createStackNavigator();
const ContactsNavigator = createStackNavigator();
const AboutUsNavigator = createStackNavigator();
const ReservationNavigator = createStackNavigator();
const FavoritesNavigator = createStackNavigator();

const AuthStack = ({ navigation }) => {
	return (
		<AuthNavigator.Navigator
			initialRouteName="Auth"
			screenOptions={stackOptions(navigation)}
		>
			<AuthNavigator.Screen
				name="Auth"
				component={Auth}
				options={{title: 'Auth'}}
			/>
		</AuthNavigator.Navigator>
	);
}

const HomeStack = ({ navigation }) => {
	return (
		<HomeNavigator.Navigator
			initialRouteName="Home"
			screenOptions={stackOptions(navigation)}
		>
	
			<HomeNavigator.Screen
				name="Home"
				component={Home}
				options={{title: 'Home'}}
			/>
		</HomeNavigator.Navigator>
	);
}

const MenuStack = ({ navigation }) => {
	return (
 		<MenuNavigator.Navigator
 			initialRouteName="Menu"
 		>

 			<MenuNavigator.Screen
 				name="Menu"
 				component={Menu}
 				options={{...stackOptions(navigation),title: 'Menu'}}
 			/>
 			<MenuNavigator.Screen
 				name="Dish Details"
 				component={Dishdetail}
 				options={{
						headerStyle: {
							backgroundColor: '#512DA8'
						},
						headerTintColor: '#fff',
						headerTitleStyle: {
							color: '#fff'
						},
						title: 'Dish Details'
				}}
 			/>
 		</MenuNavigator.Navigator>);
}

const FavoritesStack = ({ navigation }) => {
	return (
 		<FavoritesNavigator.Navigator
 			initialRouteName="Favorites"
 		>

 			<FavoritesNavigator.Screen
 				name="Favorites"
 				component={Favorites}
 				options={{...stackOptions(navigation),title: 'Favorites'}}
 			/>
 			<FavoritesNavigator.Screen
 				name="Dish Details"
 				component={Dishdetail}
 				options={{
						headerStyle: {
							backgroundColor: '#512DA8'
						},
						headerTintColor: '#fff',
						headerTitleStyle: {
							color: '#fff'
						},
						title: 'Dish Details'
				}}
 			/>
 		</FavoritesNavigator.Navigator>);
}

const ContactsStack = ({ navigation }) => {
	return (
		<ContactsNavigator.Navigator
			initialRouteName="Contact Us"
			screenOptions={stackOptions(navigation)}
		>
	
			<ContactsNavigator.Screen
				name="Contact Us"
				component={Contacts}
				options={{title: 'Contact Us'}}
			/>
		</ContactsNavigator.Navigator>);
}

const AboutUsStack = ({ navigation }) => {
	return (
		<AboutUsNavigator.Navigator
			initialRouteName="About Us"
			screenOptions={stackOptions(navigation)}
		>
	
			<AboutUsNavigator.Screen
				name="About Us"
				component={AboutUs}
				options={{title: 'About Us'}}
			/>
		</AboutUsNavigator.Navigator>);
}

const ReservationStack = ({ navigation }) => {
	return (
		<ReservationNavigator.Navigator
			initialRouteName="Reservation"
			screenOptions={stackOptions(navigation)}
		>
	
			<ReservationNavigator.Screen
				name="Reservation"
				component={Reservation}
				options={{title: 'Reservation'}}
			/>
		</ReservationNavigator.Navigator>);
}


const CustomDrawerContentComponent = (props) => (
	<DrawerContentScrollView {...props}>
		<SafeAreaView
			style={styles.container}
			forceInset={{ top: 'always', horizontal: 'never' }}
		>
			<View style={styles.drawerHeader}>
				<View style={{flex: 1}}>
					<Image source={require('./images/logo.png')}
						   style={styles.drawerImage} 
					/>
				</View>
				<View style={{flex: 2}}>
					<Text style={styles.drawerHeaderText}>Ristorante Con Funfusion</Text>
				</View>
			</View>
			<DrawerItemList {...props} />
		</SafeAreaView>
	</DrawerContentScrollView>
)

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	drawerHeader: {
		backgroundColor: '#512DA8',
		height: 140,
		alignItems: 'center',
		justifyContent: 'center',
		flex: 1,
		flexDirection: 'row'
	},
	drawerImage: {
		margin: 10,
		width: 80,
		height: 60
	}
})

class Main extends Component {

	constructor(props){
		super(props);
		this.unsubscribeNetInfo = () => {};
	}
	
	componentDidMount() {
		this.props.fetchDishes();
		this.props.fetchComments();
		this.props.fetchPromos();
		this.props.fetchLeaders();


		NetInfo.fetch()
			.then((connectionInfo) => {
				ToastAndroid.show('Initial Network Connectivity Type: '
					+ connectionInfo.type + ', strength: ' + connectionInfo.details.strength, ToastAndroid.LONG);
			});

		this.unsubscribeNetInfo = NetInfo.addEventListener(this.handleConnectivityChange);

	}

	componentWillUnmount() {
		this.unsubscribeNetInfo();
	}

	handleConnectivityChange(connectionInfo){
		switch(connectionInfo.type) {
			case 'none':
				ToastAndroid.show('You are now offline!', ToastAndroid.LONG);	
				break;
			case 'wifi':
				ToastAndroid.show('You are now connected to wifi!', ToastAndroid.LONG);	
				break;
			case 'cellular':
				ToastAndroid.show('You are now connected to cellular!', ToastAndroid.LONG);	
				break;
			case 'unknown':
				ToastAndroid.show('You are now connected to unknown connection!', ToastAndroid.LONG);	
				break;
			default:
				break;
		}
	}

	render() {
		return(
			<View style={{flex: 1,  paddingTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight }}>
    		<NavigationContainer>
				<MainNavigator.Navigator
					initialRouteName="Home"
					drawerStyle={drawerStyle}
					drawerContent={CustomDrawerContentComponent}
				>
					<MainNavigator.Screen name="Auth"
						component={AuthStack}
						options={drawerOptions('key')}
					/>
					<MainNavigator.Screen name="Home"
						component={HomeStack}
						options={drawerOptions('home')}
					/>
					<MainNavigator.Screen name="Menu"
						component={MenuStack}
						options={drawerOptions('list')}
					/>
					<MainNavigator.Screen name="Contact Us"
						component={ContactsStack}
						options={drawerOptions('address-card')}
					/>
					<MainNavigator.Screen name="About Us"
						component={AboutUsStack}
						options={drawerOptions('info-circle')}
					/>
					<MainNavigator.Screen name="Reservation"
						component={ReservationStack}
						options={drawerOptions('cutlery')}
					/>
					<MainNavigator.Screen name="Favorites"
						component={FavoritesStack}
						options={drawerOptions('heart')}
					/>
				</MainNavigator.Navigator>
    		</NavigationContainer>
			</View>
		);
	}
	
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
